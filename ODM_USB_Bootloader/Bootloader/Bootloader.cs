﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODM_USB
{
    public class BootloaderException : Exception
    {
        public BootloaderException() { }
        public BootloaderException(string message) : base(message) { }
        public BootloaderException(string message, Exception innerException) : base(message, innerException) { }
    }

    public abstract class Bootloader
    {
        #region PIC32
        public abstract void QueryPIC32();
        public abstract bool ErasePIC32(bool isRP560 = true);
        public abstract bool ProgramPIC32(HexFile hexFile, bool isRP560 = true);
        public abstract bool VerifyPIC32(HexFile hexFile);
        public abstract void StartPIC32Application();
        #endregion

        #region PIC18
        public abstract void Query();
        public abstract void Reset();
        public abstract bool Erase();
        public abstract HexFile Read();
        public abstract bool Verify(HexFile hex);
        public abstract bool Program(HexFile hex, bool programConfigs = false);
        public abstract void SignFlash();
        public abstract bool VerifySignature(HexFile hex);
        public abstract bool GetData(UInt32 address, UInt32 endAddress, out byte[] data);
        #endregion

        public abstract bool Scan();
    }
}
