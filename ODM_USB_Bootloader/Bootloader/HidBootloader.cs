﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ODM_USB
{
    public class HidBootloader : Bootloader
    {
        public HidDevice HidDevice;

        public MemoryRegionStruct[] MemoryRegions { get { return memoryRegions; } }
        public DeviceFamilyType DeviceFamily { get { return deviceFamily; } }

        #region Constants

        public const int PIC24_RESET_REMAP_OFFSET = 0x1400;
        public const int MAX_DATA_REGIONS = 6;
        public const int COMMAND_PACKET_SIZE = 65;
        const int MAX_ERASE_BLOCK_SIZE = 8196;   //Increase this in the future if any microcontrollers with bigger than 8196 byte erase block is implemented
        const byte BOOTLOADER_V1_01_OR_NEWER_FLAG = 0xA5;
        public const byte SOH = 0x01;
        public const byte EOT = 0x04;
        public const byte DLE = 0x10;

        const UInt32 ERROR_SUCCESS = 0;
        const UInt32 INVALID_HANDLE_VALUE = UInt32.MaxValue - 1;

        #endregion

        #region Bootloader Command Constants
        public enum BootloaderCommand : byte
        {
            QueryDevice = 0x02,
            UnlockConfig = 0x03,
            EraseDevice = 0x04,
            ProgramDevice = 0x05,
            ProgramComplete = 0x06,
            GetData = 0x07,
            ResetDevice = 0x08,
            SignFlash = 0x09, //The host PC application should send this command after the verify operation has completed successfully.  If checksums are used instead of a true verify (due to ALLOW_GET_DATA_COMMAND being commented), then the host PC application should send SIGN_FLASH command after is has verified the checksums are as exected.The firmware will then program the SIGNATURE_WORD into flash at the SIGNATURE_ADDRESS.
            QueryExtendedInfo = 0x0C, //Used by host PC app to get additional info about the device, beyond the basic NVM layout provided by the query device command
            GetEncryptedFF = 0xFF
        };

        public enum PIC32BootloaderCommand : byte
        {
            PIC32_READ_BOOT_INFO = 0x01,
            PIC32_ERASE_FLASH = 0x02,
            PIC32_PROGRAM_FLASH = 0x03,
            PIC32_READ_CRC = 0x04,
            PIC32_JMP_TO_APP = 0x05,
            PIC32_DLS_ERASE_FLASH = 0x06,
            PIC32_DLS_PROGRAM_FLASH = 0x07
        };
        //**************************************************************************

        //*********************** QUERY RESULTS ************************************
        public const int QUERY_IDLE = 0xFF;
        public const int QUERY_RUNNING = 0x00;
        public const int QUERY_SUCCESS = 0x01;
        public const int QUERY_WRITE_FILE_FAILED = 0x02;
        public const int QUERY_READ_FILE_FAILED = 0x03;
        public const int QUERY_MALLOC_FAILED = 0x04;
        //**************************************************************************

        //*********************** PROGRAMMING RESULTS ******************************
        public const int PROGRAM_IDLE = 0xFF;
        public const int PROGRAM_RUNNING = 0x00;
        public const int PROGRAM_SUCCESS = 0x01;
        public const int PROGRAM_WRITE_FILE_FAILED = 0x02;
        public const int PROGRAM_READ_FILE_FAILED = 0x03;
        public const int PROGRAM_RUNNING_ERASE = 0x05;
        public const int PROGRAM_RUNNING_PROGRAM = 0x06;
        //**************************************************************************

        //*********************** ERASE RESULTS ************************************
        public const int ERASE_IDLE = 0xFF;
        public const int ERASE_RUNNING = 0x00;
        public const int ERASE_SUCCESS = 0x01;
        public const int ERASE_WRITE_FILE_FAILED = 0x02;
        public const int ERASE_READ_FILE_FAILED = 0x03;
        public const int ERASE_VERIFY_FAILURE = 0x04;
        public const int ERASE_POST_QUERY_FAILURE = 0x05;
        public const int ERASE_POST_QUERY_RUNNING = 0x06;
        public const int ERASE_POST_QUERY_SUCCESS = 0x07;
        //**************************************************************************

        //*********************** VERIFY RESULTS ***********************************
        public const int VERIFY_IDLE = 0xFF;
        public const int VERIFY_RUNNING = 0x00;
        public const int VERIFY_SUCCESS = 0x01;
        public const int VERIFY_WRITE_FILE_FAILED = 0x02;
        public const int VERIFY_READ_FILE_FAILED = 0x03;
        public const int VERIFY_MISMATCH_FAILURE = 0x04;
        //**************************************************************************

        //*********************** READ RESULTS *************************************
        public const int READ_IDLE = 0xFF;
        public const int READ_RUNNING = 0x00;
        public const int READ_SUCCESS = 0x01;
        public const int READ_READ_FILE_FAILED = 0x02;
        public const int READ_WRITE_FILE_FAILED = 0x03;
        //**************************************************************************

        //*********************** UNLOCK CONFIG RESULTS ****************************
        public const int UNLOCK_CONFIG_IDLE = 0xFF;
        public const int UNLOCK_CONFIG_RUNNING = 0x00;
        public const int UNLOCK_CONFIG_SUCCESS = 0x01;
        public const int UNLOCK_CONFIG_FAILURE = 0x02;
        //**************************************************************************

        //*********************** BOOTLOADER STATES ********************************
        public const int BOOTLOADER_IDLE = 0xFF;
        public const int BOOTLOADER_QUERY = 0x00;
        public const int BOOTLOADER_PROGRAM = 0x01;
        public const int BOOTLOADER_ERASE = 0x02;
        public const int BOOTLOADER_VERIFY = 0x03;
        public const int BOOTLOADER_READ = 0x04;
        public const int BOOTLOADER_UNLOCK_CONFIG = 0x05;
        public const int BOOTLOADER_RESET = 0x06;
        //**************************************************************************

        //*********************** RESET RESULTS ************************************
        public const int RESET_IDLE = 0xFF;
        public const int RESET_RUNNING = 0x00;
        public const int RESET_SUCCESS = 0x01;
        public const int RESET_WRITE_FILE_FAILED = 0x02;
        //**************************************************************************

        //*********************** MEMORY REGION TYPES ******************************
        public enum MemoryRegionType : byte { PROGRAM_MEM = 0x01, EEDATA = 0x02, CONFIG = 0x03, USERID_MEM = 0x04, END = 0xFF };
        /*public const int MEMORY_REGION_PROGRAM_MEM = 0x01;
        public const int MEMORY_REGION_EEDATA = 0x02;
        public const int MEMORY_REGION_CONFIG = 0x03;
        public const int MEMORY_REGION_END = 0xFF;*/
        //**************************************************************************

        //*********************** HEX FILE CONSTANTS *******************************
        public const int HEX_FILE_EXTENDED_LINEAR_ADDRESS = 0x04;
        public const int HEX_FILE_EOF = 0x01;
        public const int HEX_FILE_DATA = 0x00;

        //This is the number of bytes per line of the 
        public const int HEX_FILE_BYTES_PER_LINE = 16;
        //**************************************************************************

        //*********************** Device Family Definitions ************************
        public enum DeviceFamilyType : byte { PIC18 = 1, PIC24 = 2, PIC32 = 3 };
        //**************************************************************************
        #endregion

        #region Pic32 Stuff
        /**
        * Table used for the table_driven implementation.
        *****************************************************************************/
        private readonly UInt16[] crc_table = {
            0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
            0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef
        };

        /****************************************************************************
         * Update the crc value with new data.
         *
         * \param crc      The current crc value.
         * \param data     Pointer to a buffer of \a data_len bytes.
         * \param len		Number of bytes in the \a data buffer.
         * \return         The updated crc value.
         *****************************************************************************/
        public UInt16 CalculateCrc(byte[] data, int len)
        {
            int i;
            int dataIndex = 0;
            int crc = 0;

            while (len-- > 0)
            {
                i = (crc >> 12) ^ (data[dataIndex] >> 4);
                crc = crc_table[i & 0x0F] ^ (crc << 4);
                i = (crc >> 12) ^ (data[dataIndex] >> 0);
                crc = crc_table[i & 0x0F] ^ (crc << 4);
                dataIndex++;
            }

            return (UInt16)(crc & 0xFFFF);
        }

        /// <summary>
        /// This method takes a defined structure and converts it into a raw byte array of all of the structure paramaters (in order)
        /// </summary>
        /// <typeparam name="T">Structure Type</typeparam>
        /// <param name="structure">Structure to convert to byte array</param>
        /// <returns></returns>
        private byte[] ToByteArray<T>(T structure) where T : struct
        {
            var bufferSize = Marshal.SizeOf(structure);
            var byteArray = new byte[bufferSize];

            IntPtr handle = Marshal.AllocHGlobal(bufferSize);
            Marshal.StructureToPtr(structure, handle, true);
            Marshal.Copy(handle, byteArray, 0, bufferSize);

            return byteArray;
        }

        /// <summary>
        /// Check if the incoming data packet is valid by double checking the CRC
        /// </summary>
        /// <param name="buffer">Byte array containing the bytes that were read in</param>
        private bool checkValidRXPacket(ref byte[] buffer)
        {
            bool RxFrameValid = false;
            bool Escape = false;
            List<byte> RxData = new List<byte>();
            int crc;

            for (int i = 1; i < buffer.Length; i++) //Start at 1 because the first byte is always reserved for Windows
            {
                switch (buffer[i])
                {
                    case SOH: //Start of header
                        if (Escape)
                        {
                            // Received byte is not SOH, but data.
                            RxData.Add(buffer[i]);
                            // Reset Escape Flag.
                            Escape = false;
                        }
                        else
                        {
                            // Received byte is indeed a SOH which indicates start of new frame.
                        }
                        break;

                    case EOT: // End of transmission
                        if (Escape)
                        {
                            // Received byte is not EOT, but data.
                            RxData.Add(buffer[i]);
                            // Reset Escape Flag.
                            Escape = false;
                        }
                        else
                        {
                            // Received byte is indeed a EOT which indicates end of frame.
                            // Calculate CRC to check the validity of the frame.
                            if (RxData.Count > 1)
                            {
                                crc = (RxData[RxData.Count - 2]) & 0x00ff;
                                crc = crc | ((RxData[RxData.Count - 1] << 8) & 0xFF00);
                                if ((CalculateCrc(RxData.ToArray(), (RxData.Count - 2)) == crc) && (RxData.Count > 2))
                                {
                                    // CRC matches and frame received is valid.
                                    RxFrameValid = true;
                                    //set our received bytes to our new buffer
                                    buffer = RxData.ToArray();
                                    return RxFrameValid;
                                }
                            }
                        }
                        break;


                    case DLE: // Escape character received.
                        if (Escape)
                        {
                            // Received byte is not ESC but data.
                            RxData.Add(buffer[i]);
                            // Reset Escape Flag.
                            Escape = false;
                        }
                        else
                        {
                            // Received byte is an escape character. Set Escape flag to escape next byte.
                            Escape = true;
                        }
                        break;

                    default: // Data field.
                        RxData.Add(buffer[i]);
                        // Reset Escape Flag.
                        Escape = false;
                        break;

                }
            }
            return RxFrameValid;
        }
        #endregion

        #region Structs

        #region PIC18Structs

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct MemoryRegionStruct
        {
            public MemoryRegionType Type;
            public UInt32 Address; //this is the start address
            public UInt32 Size; //this is the size of the memory region meaning the end of the address location would be address + size
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ConfigMaskStruct
        {
            public byte Config1LMask;
            public byte Config1HMask;
            public byte Config2LMask;
            public byte Config2HMask;
            public byte Config3LMask;
            public byte Config3HMask;
            public byte Config4LMask;
            public byte Config4HMask;
            public byte Config5LMask;
            public byte Config5HMask;
            public byte Config6LMask;
            public byte Config6HMask;
            public byte Config7LMask;
            public byte Config7HMask;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct QueryDeviceStruct
        {
            public byte WindowsReserved;
            public BootloaderCommand Command;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct QueryResultsStruct
        {
            public byte WindowsReserved;
            public byte Command;
            public byte BytesPerPacket;
            public DeviceFamilyType DeviceFamily;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = MAX_DATA_REGIONS)]
            public MemoryRegionStruct[] MemoryRegions;

            public byte Version_1_01_Or_Newer;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct UnlockConfigStruct
        {
            public byte WindowsReserved;
            public BootloaderCommand Command;
            public byte Setting;
        }

        const int PROGRAM_PACKET_DATA_SIZE = 58;
        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct ProgramDeviceStruct
        {
            public byte WindowsReserved;
            public BootloaderCommand Command;
            public UInt32 Address;
            public byte BytesPerPacket;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = PROGRAM_PACKET_DATA_SIZE)]
            public byte[] Data;
        }

        /*[StructLayout(LayoutKind.Sequential, Size = COMMAND_PACKET_SIZE)]
        public struct ProgramCompleteStruct
        {
            public byte WindowsReserved;
            public byte Command;
        }*/

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct GetDataStruct
        {
            public byte WindowsReserved;
            public byte Command;
            public UInt32 Address;
            public byte BytesPerPacket;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct GetDataResultsStruct
        {
            public byte WindowsReserved;
            public byte Command;
            public UInt32 Address;
            public byte BytesPerPacket;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 58)]
            public byte[] Data;
        }

        //**NOTE - THIS HAS ONLY BEEN CONFIGURED FOR PIC18, PIC 24 AND PIC 32 MAY REQUIRE DIFFERENT PARAMETERS
        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct GetExtendedQueryInfoResultsStruct
        {
            public byte WindowsReserved;
            public byte Command;
            public UInt16 BootloaderVersion;
            public UInt16 ApplicationVersion;
            public UInt32 SignatureAddress;
            public UInt16 SignatureValue;
            public UInt32 ErasePageSize;

            [MarshalAs(UnmanagedType.Struct, SizeConst = 1)]
            public ConfigMaskStruct configMaskStruct;
        }

        /*[StructLayout(LayoutKind.Sequential, Size = COMMAND_PACKET_SIZE)]
        public struct ResetDeviceStruct
        {
            public byte WindowsReserved;
            public byte Command;
        }*/

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct GetEncryptedFFResultsStruct
        {
            public byte WindowsReserved;
            public byte Command;
            public byte blockSize;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 63)]
            public byte[] Data;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct PacketDataStruct
        {
            public byte WindowsReserved;
            public BootloaderCommand Command;
            public UInt32 Address;
            public byte BytesPerPacket;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct BootloaderCommandStruct
        {
            public byte WindowsReserved;
            public BootloaderCommand Command;
        }

        #endregion

        #region PIC32Structs

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 1)]
        public struct PIC32BootloaderCommandStruct
        {
            public PIC32BootloaderCommand Command;
            public UInt16 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 3)]
        public struct QueryPIC32DeviceStruct
        {
            public PIC32BootloaderCommand Command;
            public UInt16 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct QueryPIC32ResultsStruct
        {
            public PIC32BootloaderCommand Command;
            public byte MajorVersion;
            public byte MinorVersion;
            public UInt16 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct ProgramPIC32DeviceStruct
        {
            public PIC32BootloaderCommand Command;
            public byte[] data;
            public UInt16 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 13)]
        public struct VerifyPIC32CommandStruct
        {
            public PIC32BootloaderCommand Command;
            public UInt32 StartAddress;
            public UInt32 Length;
            public UInt16 VerifyCRC;
            public UInt16 CRC;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = COMMAND_PACKET_SIZE)]
        public struct VerifyPIC32ResultsStruct
        {
            public PIC32BootloaderCommand Command;
            public UInt16 VerifyCRC;
            public UInt16 CRC;
        }

        #endregion

        #endregion

        #region Private Variables

        byte encryptionBlockSize;
        byte[] encryptedFF;

        MemoryRegionStruct[] memoryRegions;
        GetExtendedQueryInfoResultsStruct extendedQueryStruct;

        byte bootloaderState;
        byte progressStatus;
        byte versionNewerFlag;

        byte bytesPerWord;
        byte bytesPerAddressInHex;

        bool unlockStatus;

        byte bytesPerAddress = 0;
        byte bytesPerPacket = 0;

        DeviceFamilyType deviceFamily;

        #endregion

        #region Constructors

        public HidBootloader(string DeviceId)
        {
            HidDevice = new HidDevice(DeviceId);

            memoryRegions = new MemoryRegionStruct[MAX_DATA_REGIONS];

            unlockStatus = false;

            //Set the progress status bar to 0%
            progressStatus = 0;

            //Set the number of bytes per address to 0 until we perform
            //	a query and get the real results
            bytesPerAddress = 0;

        }

        public void Dispose()
        {

        }

        #endregion

        #region Device Commands

        /// <summary>
        /// Scan for a connected USB device, throws an exception if none connected
        /// </summary>
        public override bool Scan()
        {
            try
            {
                HidDevice.Scan("Pid_003c");
                return true;
            }
            catch (HidDeviceException)
            {
                return false;
            }
        }

        #region PIC18Query

        /// <summary>
        /// This function queries the attached device for the programmable memory regions
        /// and stores the information returned into the memoryRegions array.
        /// </summary>
        public override void Query()
        {
            // Attempt to connect to the HidDevice
            if (!this.Scan()) throw new BootloaderException("Device not connected");

            if (HidDevice.DevicePath == null)
                throw new BootloaderException("HID device not connected");

            //Create the write file and read file handles the to the USB device
            //  that we want to talk to
            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                using (var ReadDevice = HidDevice.GetReadFile())
                {
                    QueryDeviceStruct myCommand = new QueryDeviceStruct();
                    QueryResultsStruct myResponse = new QueryResultsStruct();

                    //Prepare the command that we want to send, in this case the QUERY
                    //  device command
                    myCommand.WindowsReserved = 0;
                    myCommand.Command = BootloaderCommand.QueryDevice;

                    //Send the command that we prepared
                    WriteDevice.WriteStructure<QueryDeviceStruct>(myCommand);

                    //Try to read a packet from the device
                    myResponse = ReadDevice.ReadStructure<QueryResultsStruct>();

                    //If we were able to successfully read from the device

                    /*#if defined(DEBUG_THREADS) && defined(DEBUG_USB)
                        DEBUG_OUT("*** QUERY RESULTS ***");
                        printBuffer(myResponse.PacketData.Data,64);
                    #endif*/

                    //for each of the possible memory regions
                    var memRegions = new List<MemoryRegionStruct>();
                    for (byte i = 0; i < MAX_DATA_REGIONS; i++)
                    {
                        //If the type of region is 0xFF that means that we have
                        //  reached the end of the regions array.
                        if (myResponse.MemoryRegions[i].Type == MemoryRegionType.END)
                            break;

                        //copy the data from the packet to the local memory regions array
                        memRegions.Add(myResponse.MemoryRegions[i]);

                        /*#if defined(DEBUG_THREADS)
                            DEBUG_OUT(HexToString(memoryRegions[i].Type,1));
                            DEBUG_OUT(HexToString(memoryRegions[i].Address,4));
                            DEBUG_OUT(HexToString(memoryRegions[i].Size,4));
                            DEBUG_OUT("********************************************");
                        #endif*/

                    }
                    memoryRegions = memRegions.ToArray();

                    /*#if defined(DEBUG_THREADS)
                        DEBUG_OUT(HexToString(memoryRegionsDetected,1));
                    #endif*/

                    //copy the last of the data out of the results packet
                    switch ((DeviceFamilyType)myResponse.DeviceFamily)
                    {
                        case DeviceFamilyType.PIC18:
                            bytesPerAddress = 1;
                            bytesPerWord = 2;
                            //ckbox_ConfigWordProgramming_restore = true;
                            break;
                        case DeviceFamilyType.PIC24:
                            bytesPerAddress = 2;
                            bytesPerWord = 4;
                            //ckbox_ConfigWordProgramming_restore = true;
                            break;
                        case DeviceFamilyType.PIC32:
                            bytesPerAddress = 1;
                            bytesPerWord = 4;
                            //ckbox_ConfigWordProgramming_restore = false;
                            break;
                        default:
                            break;
                    }
                    deviceFamily = (DeviceFamilyType)myResponse.DeviceFamily;
                    bytesPerPacket = myResponse.BytesPerPacket;
                    versionNewerFlag = myResponse.Version_1_01_Or_Newer;

                    /*#if defined(DEBUG_THREADS)
                        DEBUG_OUT("********************************************");
                        DEBUG_OUT(String::Concat("Bytes per address = 0x",HexToString(bytesPerAddress,1)));
                        DEBUG_OUT(String::Concat("Bytes per packet = 0x",HexToString(bytesPerPacket,1)));
                        DEBUG_OUT("********************************************");
                    #endif*/
                }
            }
            if (versionNewerFlag == BOOTLOADER_V1_01_OR_NEWER_FLAG)
            {
                using (var WriteDevice = HidDevice.GetWriteFile())
                {
                    using (var ReadDevice = HidDevice.GetReadFile())
                    {
                        QueryDeviceStruct myCommand = new QueryDeviceStruct();
                        extendedQueryStruct = new GetExtendedQueryInfoResultsStruct();

                        //Prepare the command that we want to send, in this case the QUERY
                        //  device command
                        myCommand.WindowsReserved = 0;
                        myCommand.Command = BootloaderCommand.QueryExtendedInfo;

                        //Send the command that we prepared
                        WriteDevice.WriteStructure<QueryDeviceStruct>(myCommand);

                        //Try to read a packet from the device
                        extendedQueryStruct = ReadDevice.ReadStructure<GetExtendedQueryInfoResultsStruct>();

                    }
                }
            }
        }

        #endregion

        #region PIC32Query
        /// <summary>
        /// This function queries the attached PIC32 device for the Bootloader version information
        /// </summary>
        public override void QueryPIC32()
        {
            // Attempt to connect to the HidDevice
            if (!this.Scan()) throw new BootloaderException("Device not connected");

            if (HidDevice.DevicePath == null)
                throw new BootloaderException("HID device not connected");

            //Create the write file and read file handles the to the USB device
            //  that we want to talk to
            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                using (var ReadDevice = HidDevice.GetReadFile())
                {
                    QueryPIC32DeviceStruct myCommand = new QueryPIC32DeviceStruct();
                    QueryPIC32ResultsStruct myResponse = new QueryPIC32ResultsStruct();

                    //Prepare the command that we want to send, in this case the QUERY
                    //  device command
                    //Option 1 -  Does not need a Windows reserved byte since this is added automatically now when building the TXFrame
                    myCommand.Command = PIC32BootloaderCommand.PIC32_READ_BOOT_INFO;
                    myCommand.CRC = CalculateCrc(ToByteArray<QueryPIC32DeviceStruct>(myCommand), 1);

                    //Send the command that we prepared
                    WriteDevice.WriteStructure<QueryPIC32DeviceStruct>(myCommand, true);

                    //Option 2
                    //List<byte> dataList = new List<byte>();
                    //dataList.Add((byte)PIC32BootloaderCommand.PIC32_READ_BOOT_INFO);
                    //UInt16 crc = CalculateCrc(dataList.ToArray(), dataList.Count);
                    //dataList.Add((byte)crc);
                    //dataList.Add((byte)(crc >> 8));

                    ////Send the command that we prepared
                    //WriteDevice.formTXPacket(dataList.ToArray());

                    //Try to read a packet from the device
                    //myResponse = ReadDevice.ReadStructure<QueryResultsStruct>();
                    var bytes = ReadDevice.ReadPIC32();


                    //If we were able to successfully read from the device
                    if (bytes != null && bytes.Length > 0)
                    {
                        if (checkValidRXPacket(ref bytes))
                        {
                            // Convert to target struct
                            // Note that it does not copy the data, it assigns the struct to the same memory as the byte array.
                            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
                            myResponse = (QueryPIC32ResultsStruct)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(QueryPIC32ResultsStruct));
                        }
                    }

                    if (myResponse.MajorVersion != 0x00) //We have data if it is not 0 (default)
                    {
                        Console.WriteLine("Successfully Queried Device!");
                        string Pic32BootloaderVersion = myResponse.MajorVersion.ToString() + "." + myResponse.MinorVersion.ToString();
                    }

                    deviceFamily = DeviceFamilyType.PIC32;
                }
            }
        }
        #endregion

        public override bool GetData(UInt32 address, UInt32 endAddress, out byte[] data)
        {
            UInt32 addressesToFetch = endAddress - address;
            bool validData = true;
            List<byte> incomingData = new List<byte>();

            //Check to avoid possible division by zero when computing the percentage completion status.
            if (addressesToFetch == 0)
                addressesToFetch++;

            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                using (var ReadDevice = HidDevice.GetReadFile())
                {
                    //First error check the input parameters before using them
                    if ((endAddress < address) || (bytesPerPacket == 0))
                    {
                        //Display Warning and exit out gracefully
                        validData = false;
                    }

                    if (validData)
                    {
                        while (address < endAddress)
                        {
                            PacketDataStruct myCommand = new PacketDataStruct();
                            GetDataResultsStruct myResponse = new GetDataResultsStruct();

                            //Prepare the command that we want to send, in this case the GET_DATA command
                            myCommand.WindowsReserved = 0;
                            myCommand.Command = BootloaderCommand.GetData;
                            myCommand.Address = address;

                            //Calculate to see if the entire buffer can be filled with data, or just partially
                            if (((endAddress - address) * bytesPerAddress) < bytesPerPacket)
                            {
                                // If the amount of bytes left over between current address and end address is less than
                                //  the max amount of bytes per packet, then make sure the bytesPerPacket info is updated
                                myCommand.BytesPerPacket = (byte)((endAddress - address) * bytesPerAddress);
                            }
                            else
                            {
                                // Otherwise keep it at its maximum
                                myCommand.BytesPerPacket = bytesPerPacket;
                            }

                            //Send the command that we prepared
                            WriteDevice.WriteStructure<PacketDataStruct>(myCommand);

                            //Try to receive the data back
                            myResponse = ReadDevice.ReadStructure<GetDataResultsStruct>();

                            //Now store the read bytes into a byte list and then once we are finished, save the bytes into a class level byte array
                            //The bootloader application will always send a packet with the max bytes per packet (in our case 58 bytes) but will only
                            //write the buffer the amount of bytes we requested (for example if 6 bytes were requested, it will return 58 bytes with only
                            //the last 6 bytes of that buffer containing actual data). This means, if we request a read less than the bytesPerPacket size 
                            //of 58, then the bytes need to be read the bottom up
                            //***NOTE: The if statement and second for loop can be removed if we keep BytesPerPacket in the command set to the max all the time,
                            //because this would read in our 6 bytes followed by 52 bytes of just garbage that we would ignore anyways in the for loop
                            //This has been tested and worked for our device but could fail in other devices so we will use the if statement below for safety.
                            if (myCommand.BytesPerPacket == bytesPerPacket)
                            {
                                for (int i = 0; i < myCommand.BytesPerPacket; i++)
                                {
                                    incomingData.Add(myResponse.Data[i]);
                                }
                            }
                            else //we requested less than the max packet size. Read the bytes in from the bottom up
                            {
                                for (int i = (bytesPerPacket - myCommand.BytesPerPacket); i < bytesPerPacket; i++)
                                {
                                    incomingData.Add(myResponse.Data[i]);
                                }
                            }

                            // Increment address by however many bytes were received divided by how many bytes per address
                            address += (uint)(myCommand.BytesPerPacket / bytesPerAddress); // "/ bytesPerAddress;" Not needed since this is just 1 byte on PIC 18 and 32, commenting would not require a cast
                        }
                    }
                }
            }
            if (incomingData.Count > 0)
            {
                data = incomingData.ToArray();
                return true;
            }
            else
            {
                data = null;
                return false;
            }
        }

        #endregion

        #region Basic Commands
        /// <summary>
        /// Send a generic packet with no response
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="packet"></param>
        private void SendCommandPacket<T>(T packet)
        {
            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                WriteDevice.WriteStructure<T>(packet);
            }
        }

        /// <summary>
        /// Resets the target device
        /// </summary>
        public override void Reset()
        {
            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                WriteDevice.WriteStructure<BootloaderCommandStruct>(new BootloaderCommandStruct
                {
                    WindowsReserved = 0,
                    Command = BootloaderCommand.ResetDevice
                });
            }
        }

        /// <summary>
        /// Resets the target device and boots into the application
        /// </summary>
        public override void StartPIC32Application()
        {
            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                WriteDevice.WriteStructure<PIC32BootloaderCommandStruct>(new PIC32BootloaderCommandStruct
                {
                    Command = PIC32BootloaderCommand.PIC32_JMP_TO_APP,
                    CRC = CalculateCrc(BitConverter.GetBytes(((Int64)PIC32BootloaderCommand.PIC32_JMP_TO_APP)).Take(1).ToArray(), 1)
                }, true);
            }
        }

        /// <summary>
        /// Erases the target device
        /// </summary>
        public override bool Erase()
        {
            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                WriteDevice.WriteStructure<BootloaderCommandStruct>(new BootloaderCommandStruct
                {
                    WindowsReserved = 0,
                    Command = BootloaderCommand.EraseDevice
                });
            }

            WaitForCommand();
            return true;
        }

        /// <summary>
        /// Erases the PIC32 target device
        /// </summary>
        public override bool ErasePIC32(bool isRP560 = true)
        {
            PIC32BootloaderCommand commandToSend = isRP560 ? PIC32BootloaderCommand.PIC32_ERASE_FLASH : PIC32BootloaderCommand.PIC32_DLS_ERASE_FLASH;
            //send the erase command.
            using (var WriteDevice = HidDevice.GetWriteFile())
            {

                WriteDevice.WriteStructure<PIC32BootloaderCommandStruct>(new PIC32BootloaderCommandStruct
                {
                    Command = commandToSend,
                    CRC = CalculateCrc(BitConverter.GetBytes(((Int64)commandToSend)).Take(1).ToArray(), 1)
                }, true);
            }

            using (var ReadDevice = HidDevice.GetReadFile())
            {
                PIC32BootloaderCommandStruct myResponse = new PIC32BootloaderCommandStruct();
                var bytes = ReadDevice.ReadPIC32();

                //If we were able to successfully read from the device
                if (bytes != null && bytes.Length > 0)
                {
                    if (checkValidRXPacket(ref bytes))
                    {
                        // Convert to target struct
                        // Note that it does not copy the data, it assigns the struct to the same memory as the byte array.
                        GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
                        myResponse = (PIC32BootloaderCommandStruct)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(PIC32BootloaderCommandStruct));
                    }
                }
                if ((isRP560 && myResponse.Command == PIC32BootloaderCommand.PIC32_ERASE_FLASH) || (!isRP560 && myResponse.Command == PIC32BootloaderCommand.PIC32_DLS_ERASE_FLASH))
                {
                    Console.WriteLine("Erase Successful!");
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Unlocks or Locks the target device's config bits for writing
        /// </summary>
        private void UnlockConfigBits(bool lockBits)
        {
            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                WriteDevice.WriteStructure<UnlockConfigStruct>(new UnlockConfigStruct
                {
                    WindowsReserved = 0,
                    Command = BootloaderCommand.UnlockConfig,

                    //0x00 is sub-command to unlock the config bits
                    //0x01 is sub-command to lock the config bits
                    Setting = (lockBits) ? (byte)0x01 : (byte)0x00
                });
            }
        }

        /// <summary>
        /// Waits for the previous command to complete, by sending a test Query packet.
        /// </summary>
        private void WaitForCommand()
        {
            using (var WriteFile = HidDevice.GetWriteFile())
            {
                //If we were able to successfully send the erase command to
                //  the device then let's prepare a query command to determine
                //  when the is responding to commands again
                WriteFile.WriteStructure<QueryDeviceStruct>(new QueryDeviceStruct
                {
                    WindowsReserved = 0,
                    Command = BootloaderCommand.QueryDevice
                });
            }

            using (var ReadFile = HidDevice.GetReadFile())
            {
                //Try to read a packet from the device
                ReadFile.ReadStructure<QueryResultsStruct>();
            }
        }

        #endregion

        #region Programming Commands

        public override HexFile Read()
        {
            return null;
        }

        /// <summary>
        /// Verify the program memory only since we do not need to verify EEPROM, CONFIG Bits, or USER_ID MEM
        /// Routine that verifies the contents of the non-voltaile memory regions in the device, after an erase/programming cycle.
        /// This function requests the memory contents of the device, then compares it against the parsed .hex file data to make sure
        /// The locations that got programmed properly match.
        /// AGAIN THIS DOES NOT VERIFY EEPROM OR CONFIG OR USER_ID MEMORY
        /// </summary>
        public override bool Verify(HexFile hex)
        {
            /* for future use if needed
            foreach (MemoryRegionStruct memStruct in memoryRegions)
            {
                if (memStruct.Type == MemoryRegionType.PROGRAM_MEM)
                {
                */
            MemoryRegionStruct memStruct = Array.Find(memoryRegions, m => m.Type == MemoryRegionType.PROGRAM_MEM);
            byte[] data;
            if (GetData(memStruct.Address, memStruct.Address + memStruct.Size, out data))
            { // continue since we now have the data
              //get the data from the Hex File
                byte[] hexData = hex.GetMemoryRegion(memStruct.Address, memStruct.Size, bytesPerAddress);

                //For this entire programmable memory address range, check to see if the data read from the device exactly
                //matches what was in the hex file.
                for (uint i = memStruct.Address; i < (memStruct.Address + memStruct.Size); i++)
                {
                    //For each byte of each device address (1 on PIC18, 2 on PIC24, since flash memory is 16-bit WORD array)
                    for (uint j = 0; j < bytesPerAddress; j++)
                    {
                        //Check if the device response data matches the data we parsed from the original input .hex file.
                        if (data[((i - memStruct.Address) * bytesPerAddress) + j] != hexData[((i - memStruct.Address) * bytesPerAddress) + j])
                        {
                            //A mismatch was detected.

                            //Check if this is a PIC24 device and we are looking at the "phantom byte"
                            //(upper byte [j = 1] of odd address [i%2 == 1] 16-bit flash words).  If the hex data doesn't match
                            //the device (which should be = 0x00 for these locations), this isn't a real verify
                            //failure, since value is a don't care anyway.  This could occur if the hex file imported
                            //doesn't contain all locations, and we "filled" the region with pure 0xFFFFFFFF, instead of 0x00FFFFFF
                            //when parsing the hex file.
                            if ((deviceFamily == DeviceFamilyType.PIC24) && ((i % 2) == 1) && (j == 1))
                            {
                                //Not a real verify failure, phantom byte is unimplemented and is a don't care.
                            }
                            else
                            {
                                //If the data wasn't a match, and this wasn't a PIC24 phantom byte, then if we get
                                //here this means we found a true verify failure.
                                //We can spit the address that failed the verification if needed but I won't do that here.
                                return false;
                            }
                        }
                    }
                }
            }
            else return false; // verify failed, we could not read any data from the device
            return true;
        }

        /// <summary>
        /// This method writes the CRC bytes to the bootloader for verification and then receives the verify acknowledgement back
        /// </summary>
        /// <param name="hexFile">The loaded hex file and data</param>
        /// <returns></returns>
        public override bool VerifyPIC32(HexFile hexFile)
        {
            //Get the data we are concerned with
            byte[] verificationData = new byte[hexFile.PIC32VerificationProgramLength + 1];
            uint flashAddress = hexFile.PIC32VerificationStartAddress;
            //This is needed so we don't increment the variable (which would cause a second verification to fail)
            uint hexVirtualFlashAddress = hexFile.virtualFlashAdrs; 
            //Loop through the data and store the relevant data in a new byte array
            for (int i = 0; i <= hexFile.PIC32VerificationProgramLength; i++)
            {
                verificationData[i] = hexFile.VirtualFlash[hexVirtualFlashAddress++];
            }
            var crc = CalculateCrc(verificationData, hexFile.PIC32VerificationProgramLength);

            //Now that we have our data, lets begin to form the data packet.
            VerifyPIC32CommandStruct myCommand = new VerifyPIC32CommandStruct()
            {
                Command = PIC32BootloaderCommand.PIC32_READ_CRC,
                StartAddress = hexFile.PIC32VerificationStartAddress,
                Length = (uint)hexFile.PIC32VerificationProgramLength,
                VerifyCRC = crc
            };

            //ADD a CRC to the packet before sending it.
            myCommand.CRC = CalculateCrc(ToByteArray<VerifyPIC32CommandStruct>(myCommand), 11);

            using (var WriteFile = HidDevice.GetWriteFile())
            {
                WriteFile.WriteStructure<VerifyPIC32CommandStruct>(myCommand, true);
            }

            using (var ReadDevice = HidDevice.GetReadFile())
            {
                VerifyPIC32ResultsStruct myResponse = new VerifyPIC32ResultsStruct();
                //Try to read a packet from the device
                //myResponse = ReadDevice.ReadStructure<QueryResultsStruct>();
                var bytes = ReadDevice.ReadPIC32();

                //If we were able to successfully read from the device
                if (bytes != null && bytes.Length > 0)
                {
                    if (checkValidRXPacket(ref bytes))
                    {
                        // Convert to target struct
                        // Note that it does not copy the data, it assigns the struct to the same memory as the byte array.
                        GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
                        myResponse = (VerifyPIC32ResultsStruct)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(VerifyPIC32ResultsStruct));
                    }
                }
                if (myResponse.VerifyCRC == crc)
                {
                    Console.WriteLine("Verification Successful!");
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Program the target device with the provided hexfile
        /// This will only work for the PIC32 devices
        /// </summary>
        /// <param name="hexFile">Hexfile containing data to program</param>
        public override bool ProgramPIC32(HexFile hexFile, bool isRP560 = true)
        {
            int lineCounter = 0; //We don't wanna send to many lines of data at once
            PIC32BootloaderCommand commandToSend = isRP560 ? PIC32BootloaderCommand.PIC32_PROGRAM_FLASH : PIC32BootloaderCommand.PIC32_DLS_PROGRAM_FLASH;
            using (var WriteDevice = HidDevice.GetWriteFile())
            {
                using (var ReadDevice = HidDevice.GetReadFile())
                {
                    ProgramPIC32DeviceStruct myCommand = new ProgramPIC32DeviceStruct();
                    PIC32BootloaderCommandStruct myResponse = new PIC32BootloaderCommandStruct();

                    //Prepare the command that we want to send, in this case the Program command
                    myCommand.Command = commandToSend;
                    List<byte> data = new List<byte>();
                    //Read each line of data, prepare the command, send the command, and then read back acknowledgment, then repeat
                    foreach (string line in hexFile.hexLineList)
                    {
                        //Prepare the data that is going to be sent, we are going send 10 lines at a time.
                        for (int i = 0; i < line.Length; i += 2)
                        {
                            string line2 = line.Substring(i);
                            if (line2.Length > 2)
                            {
                                line2 = line2.Remove(2);
                            }
                            data.Add(Convert.ToByte(line2, 16));
                        }
                        lineCounter++;
                        if (lineCounter >= 10)
                        {
                            myCommand.data = data.ToArray();
                            data.Insert(0, (byte)commandToSend);
                            myCommand.CRC = CalculateCrc(data.ToArray(), data.Count);
                            //Send the command that we prepared
                            WriteDevice.WriteProgramPacket(myCommand.data, myCommand.CRC, (byte)commandToSend);

                            //Make sure the packet was received successfully
                            var bytes = ReadDevice.ReadPIC32();

                            //If we were able to successfully read from the device
                            if (bytes != null && bytes.Length > 0)
                            {
                                if (checkValidRXPacket(ref bytes))
                                {
                                    // Convert to target struct
                                    // Note that it does not copy the data, it assigns the struct to the same memory as the byte array.
                                    GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
                                    myResponse = (PIC32BootloaderCommandStruct)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(PIC32BootloaderCommandStruct));
                                }
                            }
                            if ((isRP560 && myResponse.Command != PIC32BootloaderCommand.PIC32_PROGRAM_FLASH) || 
                                (!isRP560 && myResponse.Command != PIC32BootloaderCommand.PIC32_DLS_PROGRAM_FLASH))
                            {
                                return false;
                            }
                            //reset some variables
                            data = new List<byte>();
                            lineCounter = 0;
                        }
                    }
                    //check to see if we have any data that has yet to be sent
                    if (lineCounter > 0) //there is data still waiting to be sent
                    {
                        myCommand.data = data.ToArray();
                        data.Insert(0, (byte)commandToSend);
                        myCommand.CRC = CalculateCrc(data.ToArray(), data.Count);
                        //Send the command that we prepared
                        WriteDevice.WriteProgramPacket(myCommand.data, myCommand.CRC, (byte)commandToSend);
                    }
                }
            }

            Console.WriteLine("Programming Successful!");
            return true;
        }

        /// <summary>
        /// Program the target device with the provided hexfile
        /// </summary>
        /// <param name="hexFile">Hexfile containing data to program</param>
        /// <param name="programConfigs">If true, will attempt to program config words (WARNING: programming invalid config words could brick the device!)</param>
        public override bool Program(HexFile hexFile, bool programConfigs = false)
        {
            // Program config words first to minimise the risk that the MCU
            // is reset during programming, thus leaving the MCU in a state 
            // that can't be booted.
            if (programConfigs)
            {
                var configRegions = memoryRegions.Where(r => r.Type == MemoryRegionType.CONFIG);

                // Not all devices provide CONFIG memory regions, as it is usually not desirable to program them anyway.
                if (configRegions.Count() == 0)
                    throw new BootloaderException("Cannot program config words for this device (No CONFIG memory regions)");

                foreach (var memoryRegion in configRegions)
                {
                    ProgramMemoryRegion(hexFile, memoryRegion);
                }
            }

            // Program everything else (PROGMEM, EEDATA)
            var dataRegions = memoryRegions.Where(r => r.Type != MemoryRegionType.CONFIG);

            // This shouldn't happen in a properly configured device, but show in case it does to prevent confusion
            if (dataRegions.Count() == 0)
                throw new BootloaderException("Cannot program memory (No PROGMEM/EEDATA memory regions)");

            foreach (var memoryRegion in dataRegions)
            {
                ProgramMemoryRegion(hexFile, memoryRegion);
            }
            return true;
        }

        /// <summary>
        /// Program the target PIC memory region using the provided hex file
        /// </summary>
        /// <param name="hexFile">Hexfile containing data to program</param>
        /// <param name="memoryRegion">The target memory region to program</param>
        private void ProgramMemoryRegion(HexFile hexFile, MemoryRegionStruct memoryRegion)
        {
            using (var WriteFile = HidDevice.GetWriteFile())
            {
                byte currentByteInAddress = 1;
                bool skippedBlock = false;

                // Obtain the data related to the current memory region
                var regionData = hexFile.GetMemoryRegion(memoryRegion.Address, memoryRegion.Size, bytesPerAddress);
                int j = 0;

                // While the current address is less than the end address
                uint address = memoryRegion.Address;
                uint endAddress = memoryRegion.Address + memoryRegion.Size;
                while (address < endAddress)
                {
                    // Prepare command
                    ProgramDeviceStruct myCommand = new ProgramDeviceStruct
                    {
                        WindowsReserved = 0,
                        Command = BootloaderCommand.ProgramDevice,
                        Address = address
                    };
                    myCommand.Data = new byte[PROGRAM_PACKET_DATA_SIZE];

                    // If a block consists of all 0xFF, then there is no need to write the block
                    // as the erase cycle will have set everything to 0xFF
                    bool skipBlock = true;

                    byte i;
                    for (i = 0; i < bytesPerPacket; i++)
                    {
                        byte data = regionData[j++];

                        myCommand.Data[i + (myCommand.Data.Length - bytesPerPacket)] = data;

                        if (data != 0xFF)
                        {
                            // We can skip a block if all bytes are 0xFF.
                            // Bytes are also ignored if it is byte 4 of a 3 word instruction on PIC24 (bytesPerAddress=2, currentByteInAddress=2, even address)

                            if ((bytesPerAddress != 2) || ((address % 2) == 0) || (currentByteInAddress != 2))
                            {
                                // Then we can't skip this block of data
                                skipBlock = false;
                            }
                        }

                        if (currentByteInAddress == bytesPerAddress)
                        {
                            // If we haven't written enough bytes per address to be at the next address
                            address++;
                            currentByteInAddress = 1;
                        }
                        else
                        {
                            // If we haven't written enough bytes to fill this address
                            currentByteInAddress++;
                        }

                        //If we have reached the end of the memory region, then we
                        //  need to pad the data at the end of the packet instead
                        //  of the front of the packet so we need to shift the data
                        //  to the back of the packet.
                        if (address >= endAddress)
                        {
                            byte n;
                            i++;

                            int len = myCommand.Data.Length;
                            for (n = 0; n < len; n++)
                            {
                                if (n < i)
                                    // Move it from where it is to the back of the packet, thus shifting all of the data down.
                                    myCommand.Data[len - n - 1] = myCommand.Data[i + (len - bytesPerPacket) - n - 1];
                                else
                                    myCommand.Data[len - n - 1] = 0x00;
                            }

                            // Break out of the for loop now that all the data has been padded out.
                            break;
                        }

                    }//end for

                    // Use the counter to determine how many bytes were written
                    myCommand.BytesPerPacket = i;

                    //If the block was all 0xFF then we can just skip actually programming
                    //  this device.  Otherwise enter the programming sequence
                    if (!skipBlock)
                    {
                        //If we skipped one block before this block then we may need
                        //  to send a proramming complete command to the device before
                        //  sending the data for this command.
                        if (skippedBlock)
                        {
                            SendCommandPacket<BootloaderCommandStruct>(new BootloaderCommandStruct
                            {
                                WindowsReserved = 0,
                                Command = BootloaderCommand.ProgramComplete
                            });

                            //since we have now indicated that the programming is complete
                            //  then we now mark that we haven't skipped any blocks
                            skippedBlock = false;
                        }

                        // Write the packet data!
                        /*string debug = "";
                        foreach (byte b in myCommand.Data)
                            debug += b.ToString("x2") + " ";
                        Console.WriteLine(">>> USB OUT Packet >>>\n{0}", debug);*/

                        SendCommandPacket<ProgramDeviceStruct>(myCommand);
                    }
                    else
                    {
                        // We are skipping the block
                        skippedBlock = true;
                    }
                }//end while

                // All data for this region has been programmed
                SendCommandPacket<BootloaderCommandStruct>(new BootloaderCommandStruct
                {
                    WindowsReserved = 0,
                    Command = BootloaderCommand.ProgramComplete
                });

            }//end using
        }

        /// <summary>
        ///Successfully verified all regions without error.
        ///If this is a v1.01 or later device, we now need to issue the SIGN_FLASH
        ///command
        /// </summary>
        public override void SignFlash()
        {
            if (BOOTLOADER_V1_01_OR_NEWER_FLAG == versionNewerFlag)
            {
                //Programming has completed successfully, now we must sign_flash
                SendCommandPacket<BootloaderCommandStruct>(new BootloaderCommandStruct
                {
                    WindowsReserved = 0,
                    Command = BootloaderCommand.SignFlash
                });

                //Make sure the bootloader has received the command successfully and is again responding to commands
                WaitForCommand();
            }
        }
        ///<summary>
        ///Successfully programmed, verified, and signed the flash memory now we
        ///need to re-verify the first erase page worth of flash memory
        ///(but with the exclusion of the signature WORD address from the verify,
        ///since the bootloader firmware will have changed it to the new/magic
        ///value (probably 0x600D, or "good" in leet speak).
        ///</summary>
        public override bool VerifySignature(HexFile hex)
        {
            //Now, lets re-verify the first erase page of flash memory
            if (deviceFamily == DeviceFamilyType.PIC18)
            {
                byte[] flashData;
                UInt32 startOfEraseBlock = extendedQueryStruct.SignatureAddress - (extendedQueryStruct.SignatureAddress % extendedQueryStruct.ErasePageSize);
                MemoryRegionStruct memStruct = Array.Find(memoryRegions, m => m.Type == MemoryRegionType.PROGRAM_MEM);

                if (GetData(startOfEraseBlock, (startOfEraseBlock + extendedQueryStruct.ErasePageSize), out flashData))
                {
                    byte[] hexEraseBlockData = new byte[flashData.Length];
                    //Search through all of the programmable memory regions from the parsed .hex file data.
                    //For each of the programmable memory regions found, if the region also overlaps a region
                    //that is part of the erase block, copy out bytes into the hexEraseBlockData[] buffer,
                    //for re-verification.
                    byte[] hexData = hex.GetMemoryRegion(memStruct.Address, memStruct.Size, bytesPerAddress);
                    foreach (HexFile.MemoryBlock block in hex.blocks)
                    {
                        ulong blockEndAddress = block.size - block.startAddress;
                        //Check if any portion of the range is within the erase block of interest in the device.
                        if ((block.startAddress <= startOfEraseBlock) && (blockEndAddress > startOfEraseBlock))
                        {
                            ulong rangeSize = blockEndAddress - block.startAddress;
                            ulong address = block.startAddress;
                            UInt16 k = 0;

                            //Check every byte in the hex file range, to see if it is inside the erase block of interest
                            for (UInt16 i = 0; i < rangeSize; i++)
                            {
                                //Check if the current byte we are looking at is inside the erase block of interst
                                if (((address + i) >= startOfEraseBlock) && ((address + i) < (startOfEraseBlock + extendedQueryStruct.ErasePageSize)))
                                {
                                    //The byte is in the erase block of interst.  Copy it out into a new buffer.
                                    hexEraseBlockData[k] = block.data[i];
                                    //Check if this is a signature byte.  If so, replace the value in the buffer
                                    //with the post-signing expected signature value, since this is now the expected
                                    //value from the device, rather than the value from the hex file...
                                    if ((address + i) == extendedQueryStruct.SignatureAddress)
                                    {
                                        hexEraseBlockData[k] = (byte)extendedQueryStruct.SignatureValue;    //Write LSB of signature into buffer
                                    }
                                    if ((address + i) == (extendedQueryStruct.SignatureAddress + 1))
                                    {
                                        hexEraseBlockData[k] = (byte)(extendedQueryStruct.SignatureValue >> 8); //Write MSB into buffer
                                    }
                                    k++;
                                }
                                if ((k >= extendedQueryStruct.ErasePageSize) || (k >= hexEraseBlockData.Length))
                                {
                                    break;
                                }
                            }
                        }
                    }
                    //We now have both the hex data and the post signing flash erase block data
                    //in two RAM buffers.  Compare them to each other to perform post-signing
                    //verify.
                    for (int i = 0; i < extendedQueryStruct.ErasePageSize; i++)
                    {
                        if (flashData[i] != hexEraseBlockData[i])
                        {
                            //Post signing verification failed!
                            return false;
                            //Console.WriteLine("Sign Flash Failed!");
                            //EraseDevice();  //Send an erase command, to forcibly
                            //remove the signature (which might be valid), since
                            //there was a verify error and we can't trust the application
                            //firmware image integrity.  This ensures the device jumps
                            //back into bootloader mode always.
                        }
                    }
                }
                else return false; //Get Data failed, Could not read from UPM, reset
            }
            return true;
        }

        #endregion
    }
}
